#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <sys/time.h>
#include <pthread.h>

#define TRUE 1

using namespace std;

void* handleClient(void* arg) {
    int clientSocket = *((int*)arg);
    char buffer[8192];
    int max_clients = 30;
    while (true) {
        memset(buffer, 0, sizeof(buffer));
        int bytesReceived = recv(clientSocket, buffer, sizeof(buffer), 0);
        if (bytesReceived == -1) {
            cerr << "Error al recibir datos del cliente!" << endl;
            break;
        }
        if (bytesReceived == 0) {
            cout << "Cliente desconectado" << endl;
            break;
        }
        cout << "Cliente[" << clientSocket << "]: " << string(buffer, 0, bytesReceived) << endl;//En esta parte es lo que el cliente manda al servidor y lo muestra aca.

        // Envía el mensaje a todos los clientes conectados (incluido el cliente que envió el mensaje)
        for (int i = 0; i < max_clients; i++) {
            int sd = clientSocket;
            if (sd > 0) {
                send(sd, buffer, strlen(buffer), 0);
            }
        }
    }

    // Cierra el socket del cliente
    close(clientSocket);

    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    int clientSocket[30] = {0};
    int PORT;
    int max_clients = 30;
    int opt = TRUE;

    fd_set readfds;
    char buffer[8192];
    char *message = "Te has conectado al servidor. ¡Puedes enviar y recibir mensajes ahora!\n";

    cout << "[->] Por favor, ingresa el número de puerto para el servidor: " << endl;
    cin >> PORT;

    int serverSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (serverSocket == -1) {
        cerr << "No se pudo crear el socket" << endl;
        exit(EXIT_FAILURE);
    }

    if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0) {
        cerr << "setsockopt" << endl;
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    inet_pton(AF_INET, "0.0.0.0", &addr.sin_addr);

    if (bind(serverSocket, (sockaddr*)&addr, sizeof(addr)) < 0) {
        cerr << "Error al enlazar" << endl;
        exit(EXIT_FAILURE);
    }

    if (listen(serverSocket, 3) == 0) {
        cout << "[+] El servidor está listo para aceptar conexiones en el puerto: " << PORT << endl;
    }

    while (TRUE) {
        // Limpiar el conjunto de sockets
        FD_ZERO(&readfds);

        // Agregar el socket del servidor al conjunto
        FD_SET(serverSocket, &readfds);
        int max_sd = serverSocket;

        // Agregar los sockets de los clientes al conjunto
        for (int i = 0; i < max_clients; i++) {
            int sd = clientSocket[i];
            
            // Si el descriptor de socket es válido, agregarlo al conjunto de lectura
            if (sd > 0)
                FD_SET(sd, &readfds);

            // Actualizar el máximo descriptor de archivo
            if (sd > max_sd)
                max_sd = sd;
        }

        // Realizar la selección de sockets
        int activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);

        if ((activity < 0) && (errno != EINTR)) {
            cout << "Error en la selección" << endl;
        }

        // Manejar las conexiones entrantes
        if (FD_ISSET(serverSocket, &readfds)) {
            struct sockaddr_in clientAddr;
            socklen_t addrlen = sizeof(clientAddr);
            int newSocket = accept(serverSocket, (struct sockaddr *)&clientAddr, &addrlen);
            if (newSocket < 0) {
                cerr << "Error al aceptar la conexión" << endl;
            }
            cout << "Nueva conexión, socket fd: " << newSocket << " IP: " << inet_ntoa(clientAddr.sin_addr) << " Puerto: " << ntohs(clientAddr.sin_port) << endl;

            if (send(newSocket, message, strlen(message), 0) != strlen(message)) {
                cerr << "Error al enviar" << endl;
            }

            // Agregar el nuevo socket al arreglo de sockets de clientes
            for (int i = 0; i < max_clients; i++) {
                if (clientSocket[i] == 0) {
                    clientSocket[i] = newSocket;
                    

                    // Crear un hilo para manejar la comunicación con el cliente
                    pthread_t thread;
                    if (pthread_create(&thread, NULL, handleClient, &clientSocket[i]) != 0) {
                        cerr << "Error al crear el hilo" << endl;
                        close(newSocket);
                        break;
                    }

                    // Desacoplar el hilo para liberar los recursos después de que termine
                    pthread_detach(thread);

                    break;
                }
            }
        }
    }

    // Cerrar el socket del servidor
    close(serverSocket);

    return 0;
}
