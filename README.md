### Autor: Martin Riffo  y Andres Rojas



# README.md




**Descripcion del archivo**

Este repositorio contiene los códigos fuente de una conexion cliente servidor basado en sockets. También se incluye un Makefile para facilitar la compilación de los programas. Puede correr multiples clientes en el servidor.




## Contenido del repositorio

El repositorio contiene los siguientes archivos:

1. `server.cpp`: El código fuente del servidor de chat.
2. `cliente.cpp`: El código fuente del cliente de chat.
3. `Makefile`: Un archivo Makefile para compilar los programas.

## Requisitos

- Se requiere un compilador de C++ compatible con el estándar C++11.
- El sistema operativo debe ser compatible con las funciones y bibliotecas estándar de sockets.

## Compilación

Para compilar el servidor y el cliente, puedes utilizar el archivo Makefile proporcionado. Asegúrate de que el Makefile y los archivos de código fuente estén en el mismo directorio. Abre una terminal en el directorio y ejecuta el siguiente comando:

```
$ make
```

Esto compilará tanto el servidor como el cliente utilizando las opciones de compilación y enlazado definidas en el Makefile.

Si deseas compilar solo el servidor o el cliente por separado, puedes ejecutar los siguientes comandos:

```
$ make server
```

o

```
$ make cliente
```

## Uso

Una vez compilados, puedes ejecutar el servidor y el cliente por separado en diferentes terminales o máquinas.

### Servidor

Ejecuta el siguiente comando para iniciar el servidor:

```
$ ./server
```

El servidor estará listo para aceptar conexiones de clientes en el puerto especificado.

### Cliente

Ejecuta el siguiente comando para iniciar un cliente:

```
$ ./cliente
```

El cliente te pedirá ingresar la dirección IP y el puerto del servidor al que deseas conectarte. Después de establecer la conexión, puedes enviar mensajes al servidor y recibir mensajes de otros clientes conectados al mismo servidor.

## Limpieza

Si deseas eliminar los archivos binarios generados durante la compilación, puedes ejecutar el siguiente comando:

```
$ make clean
```

Esto eliminará los archivos `server` y `cliente` del directorio.



