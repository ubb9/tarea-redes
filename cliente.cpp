#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <sys/time.h>

using namespace std;

int main()
{
  int port;
  int clientSocket = socket(AF_INET, SOCK_STREAM, 0);  // Crear un socket del cliente
  if (clientSocket == -1){
    return 1;
  }

  cout << "Ingresa la dirección del puerto al que deseas conectarte: " << endl;
  cin >> port;
  string ipAdress = "127.0.0.1";  // Dirección IP del servidor al que te deseas conectar

  sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  inet_pton(AF_INET, ipAdress.c_str(), &addr.sin_addr);

  int connectRes = connect(clientSocket, (sockaddr*)&addr, sizeof(addr));  // Conectar el socket del cliente al servidor
  if (connectRes == 0) {
    cout << "¡Conectado al servidor!" << endl;
  }

  if (connectRes == -1) {
    return 1;
  }

  char buf[4096];
  string userInput;

  do {
    getline(cin, userInput);  // Leer la entrada del usuario

    // Enviar los datos al servidor
    int sendRes = send(clientSocket, userInput.c_str(), userInput.size() + 1, 0);

    if (sendRes == -1) {
      cout << "¡No se pudo enviar al servidor! ¡Ups!" << endl;
      continue;
    }
  } while (true);
  
  return 0;
}
