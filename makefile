CC = g++
CFLAGS = -Wall -Wextra -pedantic -std=c++11
LDFLAGS = -lpthread


all: server cliente

server: server.cpp
	$(CC) $(CFLAGS) $< -o $@ $(LDFLAGS)

cliente: cliente.cpp
	$(CC) $(CFLAGS) $< -o $@


.PHONY: clean

clean:
	rm -f server client
